const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, 'tsconfig.json'),
  [/* mapped paths to share */]);

module.exports = {
  output: {
    uniqueName: "mhClientTemplate",
    publicPath: "auto",
    scriptType: 'text/javascript'
  },
  optimization: {
    runtimeChunk: false
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  plugins: [
    new ModuleFederationPlugin({
        name: "mhClientTemplate",
        filename: "remoteEntry.js",
        exposes: {'./Module': './src/app/app.module.ts'},
        shared: share({
          "@angular/core": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/common": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/animations": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/common/http": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/router": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/forms": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/core": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/input": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/form-field": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/autocomplete": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/dialog": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/tooltip": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/snack-bar": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/icon": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/select": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/datepicker": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/material/menu": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@ngx-translate/core": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.1.2 <14.0.1' },
          "@angular/cdk": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/cdk/overlay": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/cdk/portal": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@angular/cdk/drag-drop": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "@ngxs/store": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=12.0.4' },
          "rxjs": { singleton: true, strictVersion: true, eager: true, requiredVersion: '>=6.6.3 <8.0.0' },
          "mht-client-lib": { singleton: false, strictVersion: false, eager: true, requiredVersion: 'auto' },
          ...sharedMappings.getDescriptors()
        })

    }),
    sharedMappings.getPlugin()
  ],
};
